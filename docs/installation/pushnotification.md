---
id: pushnotification
title: Push-Notifications
---

## **General Requirements**

* Changed the app-identifier mentioned in [Setting up the Mobile App](../installation/mobileapp/setup#changing-the-project-name-and-the-app-identifier).

## **Configuration Android**

***

### Create an Firebase Project

* Go to [Firebase Console](https://console.firebase.google.com/)
* Create a new Firebase project (name isn't that important)
* "Set up Google Analytics for my project" has to be selected
* Select "Default Account for Firebase".

### Add Android app

* Add Android-App to the Firebase project (Android icon in the overview)
* Fill in the previously new defined app-identifier
* Download the new *google-services.json* file.
* Replace the old *google-services.json* file with the new one (Path: *android/app/google-services.json*)

### Modify Client Config Files

* First go to the "Settings" -> "Cloud Messaging" in Firebase.
* There you have a legacy server key (server) and sender-id (client).
* Open the App Config file (Path: *App/Config/AppConfig.js*) in an editor.

```properties*
# Change it to true
automaticallyRequestPushPermissions: false,

* Put in here the sender-id
androidSenderId: '1234567890',
```

### Modify Server Config Files

* Connect to the server
* Open the Server Config file (Path: *mobilecoach-server/mc_global/mc_data/configuration.properties* in vim (editor)

```
cd mc_global/mc_data
sudo vim configuration.properties
```

* Then you have to press "a" to activate write mode.  
* Change the variables as following.

```properties
# Change it to "true"
pushNotificationsActive = false

# Change it to "true"
pushNotificationsAndroidActive = false

# Has to be false (Encrypted has a incident at the moment)
pushNotificationsAndroidEncrypted = false

# Change the Key (Insert here the created legacy server key from Firebase)
pushNotificationsAndroidAuthKey = 123456789ABC
```

* Press "Esc" and write ```:wq!``` to save the config file.
* Go back to mobilecoach-server folder and restart docker.

```
cd /opt/mobilecoach-server
sudo docker-compose restart
```

## **Configuration iOS**

***

### Requirements

* Apple Developer Account (the paid one)
* Installed *KeyChain Access* (or similar one)

### Creating a Certificate Signing Request

* Open your KeyChain Access tool (or similar one)
* Main Menu -> Certificate Assistent -> Request a Certificate From a Certificate Authority
* Enter your Certificate Information (eMail), check *Saved to desk* and save the file.

### Creating Certificate

* Log in to your Apple Developer account.
* Go to Overview -> Certificates, Identifiers & Profiles
* Go to the Identifiers and select your project identifier (e.g. ch.ethz.cdhi.mobilecoach).
* Scroll down to *Push Notificaton*, check it if it's unchecked. Click *Configure*.
* Click *Create Development SSL Certificate*.
* Choose the file you created in the first step.
* Download the Certificate.
* Add the Certificate to KeyChain Acces with a double-click. Now it should be in your KeyChain application.
* Right-click on the new Certificate and choose *Export*.
* Give it a name and click *save*. It should a file with the *.p12* ending.
* You have to type in a password. (Note: Remember it ;-))
* Export it.

### Transfer the certificate to the server

* It's a bit tricky, because you often can't directly move it to the correct folder (Permissions). First try the following command.

```
scp -rp [PATH_TO_CERTIFICATE] [USERNAME]@[HOSTNAME]:[PATH_TO_MC_DATA]
```

For example:

```
scp -rp /Users/sgfeller/Documents/Certificats-workshop-test sgfeller@workshop-cdhi.ethz.ch:/opt/mobilecoach-server-backup/mc_global/mc_data
```

* If you get "Permission denied" you have to move the Ceritifacte first into your personal folder. Here an example:

```
scp -rp /Users/sgfeller/Documents/Certificates-workshop-test.p12 sgfeller@workshop-cdhi.ethz.ch:/local/home/sgfeller
```

* You have to connect to the server and move the file now in the MC_Data folder. Here an example.

```
sudo mv Certificates-workshop-test.p12 /opt/mobilecoach-server-backup/mc_global/mc_data/
```

### Change server config file and restart docker container

* Connect to the server
* Open the Server Config file (Path: *mobilecoach-server/mc_global/mc_data/configuration.properties* in vim (editor)

```
cd mc_global/mc_data
sudo vim configuration.properties
```

* Change the following variables. (Activate Write-Mode with pressing *a*)

```properties
# Change it to "true"
pushNotificationsActive = false

# Change it to "true"
pushNotificationsIOSActive = false

# Has to be false (Encrypted has a incident at the moment)
pushNotificationsIOSEncrypted = false

# Change it to your app identifier
pushNotificationsIOSAppIdentifier = com.mycompany.myapp

# Change the Certificate name
pushNotificationsIOSCertificateFile = /mc_data/push-http2.p12

# Change it to the password for the certificate (Step 2)
pushNotificationsIOSCertificatePassword = abc123topsecret
```

* Save the file and go to the project folder. (mobilecoach-server)
* Restart the docker containers.

```
cd /opt/mobilecoach-server
sudo docker-compose restart
```

:::caution
If you test your app on TestFlight or you want to publish it you have to change the variable 'pushNotificationsProductionMode' to true!
:::
