---
id: limesurvey
title: Limesurvey
---

## **Requirements**

___
Server:

* Linux server
* Installed Mobile-Coach Server (Link)
* Ports 443, 8080 (if 443 is busy) open
* Docker installed (version 18.0.3 +)
* Certbot installed (for SSL certificates)
* Docker-compose installed v1.3+
* Openssl and openjdk installed
* Git installed
* Sudo access

## **Configuration**

___

### Connect to the server

* Open your terminal (Mac) / CMD (Windows)

```
ssh [USERNAME]@[HOSTNAME]
```

For example:

```
ssh sgfeller@workshop-cdhi.ethz.ch
```

Optional but recommended:
Create a new folder in the root directory where you afterwards pull the config.
For example:

```
cd /.
sudo mkdir /opt/
cd opt
```

### Pulling the configs

Pull the configuration and move into the *mobilecoach-server* folder.  
The rest of the commands in the following sections need to be run in folder *mobilecoach-server*

```
sudo bash
git clone https://bitbucket.org/mobilecoach/mobilecoach-limesurvey-server.git
cd mobilecoach-limesurvey-server
```

### Linking Letsenscrypt

If you already setup the Certificates for the mobilecoach-server, you can link them.

```
ln -s ../mobilecoach-server/letsencrypt/ letsencrypt
```

### Configuring the docker-compose file

Four details are required for the configuration.
Hostname, Port which LimeSurvey should use, Password of the root, Password of the user (for the login)

```
export domain=[HOSTNAME]
export limesurvey_port=[PORT_LIMESURVEY]
export mysqlrootpass=[ROOT_PASSWORD]
export mysqluserpass=[USER_PASSWORD]
```

For example:

```
export domain=workshop-mobilecoach1.ch
export limesurvey_port=443
export mysqlrootpass=b2600fcc48703
export mysqluserpass=b6fd03d8dff5f
```

After that you just have to run these commands and the file get modified.

```
find . -type f -exec sed -i 's/[hostname]/'"$domain"'/g' {} +
find . -type f -exec sed -i 's/[port_limesurvey]/'"$limesurvey_port"'/g' {} +
find . -type f -exec sed -i 's/[mysql_root_password]/'"$mysqlrootpass"'/g' {} +
find . -type f -exec sed -i 's/[mysql_user_password]/'"$mysqluserpass"'/g' {} +
```

### Starting the Docker Container

Starting the container for the first time

```
docker-compose up -d
```

Stopping, starting & restarting

```
docker-compose stop
docker-compose start
docker-compose restart
```

Stopping and removing the containers

```
docker-compose down
```

### Setting LimeSurvey up

You can access the LimeSurvey WebApp at https://[HOSTNAME]/limesurvey. Eg. https://workshop-cdhi.ethz.ch/limesurvey  
> **Note:** The default admin username is *mc* and the password is the [USER_PASSWORD] you defined above.

* Give a database name
* This setup can also be used with an existing database, without loss of data.
* Type in the admin username, password and email
* Go to "administration" and login

From next time use the same url: https://[HOSTNAME]/limesurvey/admin

## To be displayed

* Adding new user
* Try in eth server
* Scripts to add
* Parameters to pass
* Sending variables to MC
* Images in limesurvey
* Commands in MC
