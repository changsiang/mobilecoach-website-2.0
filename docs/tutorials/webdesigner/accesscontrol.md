---
id: accesscontrol
title: Access Control
---

## Overview

Under the menu item 'Access Control' new users can be created, roles can be assigned to them and user password can be set.

![alt text](/img/accessmgmt.png 'Access Control Screenshot')

When you create a new user, you have to give them the correct role and type in a first password. After the first login, the user can change his/her password under 'Account -> Set Password'.

**Important!** When you select the role Author, you also have to assign them to the intervention. You can do this in the intervention under 'Access'. See the following image.

![Intervention Access](/img/int-access.png 'Intervention Access')

## Roles

The following roles exist:

### Admin

As admin you have all rights, so you should assign this role carefully. You see all interventions and have access to all data. In addition, the 'Access Control' can be managed, i.e. create new users, change passwords etc.

### Author

The role Author has only access to the given intervention, as you can see above. An Author has all functionality in desinging/changing the intervention. But there are some limitations:

- can't create, duplicate, delete or import an intervention
- can't use the internalization feature
- no 'Access Control' menu item
- no 'Access' tab in the intervention
- can't unlock an locked intervention

### Team Manager

This role isn't used in the MobileCoach Web Designer, it's an account for the Cockpit. Therefore you don't need it right know. We will update this section when we revised the Cockpit and add a installation guide.
