---
id: decisionpoint
title: Decision points
---

With a Decision Point (DP) you can add additional rules, calculate values and jump to different messages and even different Micro Dialogs.

Click on “New Decision Point” to create it. Here you can add now different rules. You can also give the decision point a description under 'Comment'.

![Decision point](/img/decision-point.png 'Decision point')

## Decision Point rule options

![Decision point rule](/img/decision-point-rule.png 'Decision point rule')

| Options | Explanation |
|-------|-------|
| Comment | You can add a description for your rule. |
| Rule X with placeholders | The rule condition. See [rule conditions](../rules/ruleconditions.md) for all possibilites. |
| Store result to variable | Result of the condition (e.g. create text) can be stored in a variable. |
| Leave DP after this rule if rule result is TRUE | You can add several rules to your DP, so if you want to stop/exit these if this rule is TRUE, select it. |
| Stop this complete micro dialog after this rule if rule is TRUE | You can stop the entire micro-dialog with this option. |
| Jump to other dialog if TRUE | You can select another micro dialog for jumping to if the condition is TRUE |
| Jump to dialog message if TRUE | You can select a micro dialog message for jumping to if the condition is TRUE |
| Jump to dialog message if FALSE | You can select a micro dialog message for jumping to if the condition is FALSE |

## Nested rule trees

You can also nest rules. Therefore click on the rule and click “New” to add a nested rule. It then looks like this:

![Decision point nested rules](/img/decision-point-nested-rules.png 'Decision point nested rules')

## Examples

### Jumping because of a variable value

You have to create the condition and check for your values. For instance you want to jump to a different micro dialog when $group is equal to “Test”.

![Decision point jumping variable](/img/decision-point-jumping-variable.png 'Decision point jumping variable')

### Jumping everytime (not depending of a variable)

If you want to debug or just jump every time (not because of a variable value) you can just select “calculate value but result is always true” or “create text but result is always true”. So this rule is TRUE everytime. So you can easily use the “Jump to other dialog if TRUE/ Jump to dialog message if TRUE” functions.

![Decision point jumping everytime](/img/decision-point-jumping-everytime.png 'Decision point jumping everytime')

### Calculating variables (e.g. Score)

You can also calculate variables with decision points (or rules in general). Here an example:

![Decision point calculate](/img/decision-point-calculate.png 'Decision point calculate')
