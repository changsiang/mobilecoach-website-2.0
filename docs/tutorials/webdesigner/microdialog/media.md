---
id: media
title: Media Objects (image, video, audio)
---

In the Web Designer you can also add your own media files. If you only want to link online ressources like videos (from youtube, vimeo, etc.) you can use the show-web [command](commands.md).

First add the media object with the “Choose file” button and then upload it. Then you have to add the variable $systemLinkedMediaObject to a the message itself. It’s like the placeholder for the media object. At this position it will be shown. If you want to show a longer text like a 'informed consent' in html, you can use the show-info [command](commands.md).

![Add media object](/img/media-object.png 'Add media object')

### Supported media files

**Image files:** PNG or JPEG  
**Audio files:** AAC or M4A  
**Video files:** MP4/H.264 or AAC audio  
**HTML files:** HTM/HTML
