---
id: commands
title: Commands
---

## Usage

Commands are sent from the Designer to the mobile app and will trigger some actions.

There are several possible commands available. You have to write the command into the text of a message.
Be aware that you have to check the option **This message is a command** in the message options. Also you can’t have normal text and a command in the same message. Use different messages for that.

## Availabe commands

### Wait

**Command:** wait [seconds to wait]  
**Example:** wait 5

**Explanation:** You can stop the chat with this command for the amount of seconds you type in. You can use this for instance to wait until they watched a video, read a text, gave permissions etc.

### Show-web

**Command:** show-web [url] [button text]  
**Example:** show-web <https://www.youtube.com/watch?v=FHH6hIc2GyE> funny cat video

**Explanation:** If you want to refer to different websites, videos, surveys, etc. you can use this command. There will be a text bubble with the button text the participant can click. There will be a web-view in the App. So they will not leave the app, it opens directly in it.

### Show-link

**Command:** show-link [url] [button text]  
**Example:** show-link <https://mobile-coach.eu> MC Website

**Explanation:** Generates a button that will send the user to the linked site. This will leave the app and open a browser window.

### Request-push-permissions

**Command:** request-push-permissions  
**Example:** request-push-permissions

**Explanation:** Requests the participant if he wants to activate push-notifications. For now, android users will not get asked, they automatically accept. iOS users have to accept or deny this request. Best practice is to wait for the iOS users some seconds (wait-command).

### Show-info

**Command:** show-info [text]  
**Example:** show-info appinfo

**Explanation:** You can show an 'info-card' in your chat. This info-card you can create through html. We mostly use this command for the 'informed consent' or 'Terms and Conditions'. So when the participant has to read a bunch of text.

**Screenshot:** ![Show-info command](/img/intervention-command-showinfo.png 'Show-info Command')

```html
<button>Test Info</button>

<h1>Show Info Test</h1>
<p>This is a test. You could display your informed consent, or your 'Terms and Conditions' here.</p>
```

### TODO

- show-infoCardsLibrary-info
- show-infoCardsLibrary
- activate-dashboard-chat
- activate-infoCardsLibrary
- activate-mediaLibrary
- add-video
