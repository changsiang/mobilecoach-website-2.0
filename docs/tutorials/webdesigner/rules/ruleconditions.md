---
id: ruleconditions
title: Rule conditions (create text, calc value etc.)
---

There are four different rule classes:

* Calculation (calculable term)
* Text Operation (stitched text)
* Date Operation (date)
* Others

---

## Calculation

Here you can calculate decimal numbers (e.g. 2.5, empty value is 0) and compare them.
Example:

```
($participantScore * 2) - 20
```

**calculate value but result is always true**

Description: Calculate a value but the result is always true. Mostly used if you only want to calculate something without any comparison.  

Result Value: calculated decimal value  
Comparison Result: TRUE  

**calculate value but result is always false**

Description: Calculate a value but the result is always false. Mostly used if you only want to calculate something without any comparison.  

Result Value: calculated decimal value  
Comparison Result: FALSE  

**calculated value is smaller or equal than**

Description: This is like an 'x <= y' operation.  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value is smaller than**

Description: This is like an 'x < y' operation.  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value equals**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value not equals**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value is bigger or equal than**

Description: This is like an 'x >= y' operation.  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value is bigger than**

Description: This is like an 'x > y' operation.  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value matches regular expression**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculated value not matches regular expression**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculate amount of select many values**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculate sum of select many values and true if smaller than**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculate sum of select many values and true if equals**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

**calculate sum of select many values and true if bigger than**

Description:  

Result Value: calculated decimal value  
Comparison Result: TRUE / FALSE  

---

## Text Operation

Here you can merge texts or read-out variables and compare them.
The most used Text Operation is 'create text but result is always true'. You can use it so save a variable or general text into another one.

**create text but result is always true**

Description: Most used operation to save text and/or another variable into a variable. Without any comparisation.  

Result Value: stitched text  
Comparison Result: TRUE  

**create text but result is always false**

Description: Most used operation to save text and/or another variable into a variable. Without any comparisation.  

Result Value: stitched text  
Comparison Result: FALSE  

**text value equals**

Description: Used to directly compare text.  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value not equals**

Description: Used to directly compare text.  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value matches regular expression**

Description: Regular expressions very useful to compare user-input or if participant can select multiple answers.  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value not matches regular expression**

Description: Regular expressions very useful to compare user-input or if participant can select multiple answers.  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value from select many at position**

Description:  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value from select many at random position**

Description: Can be used to randomise text from the digital coach. It selects a text-block from a random position.  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value from json by json path**

Description:  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value matches key**

Description:  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

**text value not matches key**

Description:  

Result Value: stitched text  
Comparison Result: TRUE / FALSE  

---

## Date Operation

**date difference value equals**

Description:  

Result Value: none  
Comparison Result: TRUE = fits / FALSE = fits not

**calculate date difference in days and true if zero**

Description: Calculating the date difference and checking the difference  

Result Value: date difference in days  
Comparison Result: TRUE=0 / FALSE!=0  

**calculate date difference in months and true if zero**

Description: Calculating the date difference and checking the difference  

Result Value: date difference in months  
Comparison Result: TRUE=0 / FALSE!=0  

**calculate date difference in years and true if zero**

Description: Calculating the date difference and checking the difference  

Result Value: date difference in years  
Comparison Result: TRUE=0 / FALSE!=0  

**calculate date difference in days and always true**

Description:  

Result Value: date difference in days  
Comparison Result: TRUE  

**calculate date difference in months and always true**

Description:  

Result Value: date difference in months  
Comparison Result: TRUE  

**calculate date difference in years and always true**

Description:  

Result Value: date difference in years  
Comparison Result: TRUE  

---

## Others

**starts iteration from x up to y and result is current**

Description:  

Result Value:  
Comparison Result:  

**starts reverse iteration from x down to y and result is current**

Description:  

Result Value:  
Comparison Result:  

**check value in variable accross interventions and true if duplicate found**

Description:  

Result Value:  
Comparison Result:  

**execute javascript in x and store values but result is always true**

Description: This can be used for executing javascript within the Designer. An usecase could be to calculate the time between last login of the participant => for escalation-mechanisms.

Result Value:  
Comparison Result:  
