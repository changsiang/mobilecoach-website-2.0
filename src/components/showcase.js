import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  min-height: 200px;
  padding: 2.5rem 0;
  margin-bottom: -1px;
  border-top: 1px solid rgb(238, 238, 238);
  border-bottom: 1px solid rgb(238, 238, 238);
  transition: all 0.2s ease 0s;
  &:hover {
    box-shadow: rgba(0, 0, 0, 0.04) 0px 5px 40px;
  }
`;

const ShowcaseTitle = styled.h3`
  font-size: 30px;
  font-weight: 500;
`;

const ShowcaseImage = styled.div`
  text-align: center;
`;

const ShowcaseText = styled.div`
  ${(props) => (props.reversed ? 'padding-right' : 'padding-left')}: 70px;
`;

const ShowcaseContent = styled.div`
  max-width: 960px;
  margin: 0 auto;
  padding: 50px 25px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  & > * {
    width: 50%;
    flex-shrink: 1;
  }
  & > ${ShowcaseImage} {
    width: 50%;
    flex-shrink: 0;
    text-align: ${props => props.reversed ? 'right' : 'left'};
    & > img {
      max-width: 90%;
    }
  }
  @media only screen and (max-width: 996px) {
    width: 100%;
    flex-wrap: wrap;
    ${props => props.reversed ? '' : 'flex-direction: column-reverse;'}
    & > ${ShowcaseImage},
    & > ${ShowcaseText} {
      width: 100%;
      padding: 0;
      text-align: center;
    }
    & > ${ShowcaseText} {
      padding-bottom: 50px;
    }
    & > ${ShowcaseImage} > img {
      max-width: 60%;
    }
  }
`;

export function Showcase({reversed, title, img, text}) {
  const left = <ShowcaseImage>{img}</ShowcaseImage>;
  const right = (
    <ShowcaseText reversed={reversed}>
      <ShowcaseTitle>{title}</ShowcaseTitle>
      {text}
    </ShowcaseText>
  );

  return (
    <Container>
      <ShowcaseContent reversed={reversed}>
        {reversed ? (
          <>
            {right}
            {left}
          </>
        ) : (
          <>
            {left}
            {right}
          </>
        )}
      </ShowcaseContent>
    </Container>
  );
}