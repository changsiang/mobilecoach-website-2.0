import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
import {Showcase} from '../components/showcase';
import Image from '@theme/IdealImage';

const features = [
  {
    title: <>Digital health interventions</>,
    imageUrl: 'img/undraw_lifestyle.svg',
    description: (
      <>
        MobileCoach was developed as on open-source platform for the design
        of scalable digital health interventions.
      </>
    ),
  },
  {
    title: <>Chat-based coaching</>,
    imageUrl: 'img/undraw_chatting.svg',
    description: (
      <>
        Simple to participate in the intervention thanks to a chat-based 
        approach similar to common communication tips.
      </>
    ),
  },
  {
    title: <>Rule-based logic</>,
    imageUrl: 'img/undraw_in_progress.svg',
    description: (
      <>
        The structure of a digital intervention is rule-based and visualized via 
        the MobileCoach Designer. Therefore no programming knowledge is required.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Get Started`}
      description="The Open Source Digital Intervention Platform">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/about/what-is-mc')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        <Showcase
          img={
            <Image
              img={require('../../static/img/mobileapp.png')}
              alt="Annotations"
              loading="lazy"
            />
          }
          title="MobileCoach Mobile App"
          text={
            <>
              <p>
                Mobile App written in React Native and available for Android and iOS. It's chat-based and has many features, like media library, FAQ, etc.
              </p>
              <p>
                The Mobile App is used by the participant of the digital intervention.
              </p>
            </>
          }
        />
        <Showcase
          reversed={true}
          img={
            <Image
              img={require('../../static/img/webdesigner.png')}
              alt="Annotations"
              loading="lazy"
            />
          }
          title="MobileCoach Web Designer"
          text={
            <>
              <p>
                The 'brain' of the digital intervention. Here you can create your rule-based intervention, add variables, export data etc.
              </p>
              <p>
                The Designer is used by the intervention designer / researcher. 
              </p>
            </>
          }
        />
        <Showcase
          img={
            <Image
              img={require('../../static/img/cockpit.png')}
              alt="Annotations"
              loading="lazy"
            />
          }
          title="MobileCoach Web Cockpit"
          text={
            <>
              <p>
                The Cockpit can be used to monitor the participants of the intervention. Additionally you can chat directly with the participant in a second channel.
              </p>
              <p>
                The Cockpit is mostly used by additional health professionals, researcher or general study supporter.
              </p>
            </>
          }
        />
      </main>
    </Layout>
  );
}

export default Home;
